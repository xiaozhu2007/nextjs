module.exports = {
    exportPathMap: function() {
      return {
        '/': { page: '/' },
        '/api': { page: '/404' },
        '/about': { page: '/about' }
      }
    }
  }