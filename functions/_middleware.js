const errorHandler = async ({ next }) => {
  try {
    return await next()
  } catch (err) {
    return new Response(`Oops! ${err.message}\n${err.stack}`, { status: 500 })
  }
}

const head = async ({ next }) => {
  const response = await next()
  response.headers.set('X-Status', 'Middleware Enabled!')
  response.headers.set('X-By', 'Pig2333')
  return response
}

export const onRequest = [
  errorHandler,
  head
]