// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
export default (req, res) => {
    res.statusCode = 200
    res.json({
        Name: 'Pig2333',
        Age: '21',
        Playing: 'Minecraft',
        WantToTellYou: 'Test'
    })
}