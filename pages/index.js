import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>小猪API开放平台</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          欢迎来到 <a href="/">小猪API开放平台</a> !
        </h1>
        <p className={styles.description}>
          开始？单击<a href="/">这里</a>
        </p>
        <div className={styles.grid}>
          <a href="/use.html" className={styles.card}>
            <h3>文档 &rarr;</h3>
            <p>找到一些API和有用的参数&nbsp;&nbsp;&nbsp;&nbsp;</p>
          </a>

        </div>
      </main>

      <footer className={styles.footer}>
      </footer>
    </div>
  )
}
